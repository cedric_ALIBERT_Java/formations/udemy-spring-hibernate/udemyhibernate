package fr.cedricalibert.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Student;

public class ReadStudentDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			//use the session object to save Java object
			
			//create a student object
			System.out.println("Create new student object");
			Student student = new Student("Cédric", "ALIBERT", "test@test.fr");
			
			//start a transaction
			session.beginTransaction();
			
			//save the student object
			System.out.println("Save the student");
			session.save(student);
			
			//commit transaction
			session.getTransaction().commit();
			System.out.println("Done");
			
		}
		finally {
			factory.close();
		}

	}

}
