package fr.cedricalibert.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Student;

public class QueryStudentDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			//use the session object to save Java object
			
			//create a student object
			System.out.println("Create new student object");
			Student student = new Student("CédricRead", "ALIBERTRead", "testRead@test.fr");
			
			//start a transaction
			session.beginTransaction();
			
			//save the student object
			System.out.println("Save the student");
			System.out.println(student);
			session.save(student);
			
			//commit transaction
			session.getTransaction().commit();
			
			// find out the new primary Key
			System.out.println("Save student, Primary Key : "+ student.getId() );
			
			//get a new session and start transaction
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			System.out.println("\nGetting student with the id "+student.getId());
			
			// Read the student
			Student myStudent = session.get(Student.class, student.getId());
			
			System.out.println("Get completed : "+student);
			
			//commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done");
			
		}
		finally {
			factory.close();
		}

	}

}
