package fr.cedricalibert.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Student;

public class UpdateStudentDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			//use the session object to update Java object
			int studentId = 1;
			
			//start a transaction
			session.beginTransaction();
			
			System.out.println("\nGetting Student with ID "+studentId);
			
			Student myStudent = session.get(Student.class, studentId);
			
			System.out.println("Updating student");
			myStudent.setFirstName("TestUpdate");
			
			//commit transaction
			session.getTransaction().commit();
			
			// Get a new session
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			//update email for all students
			System.out.println("\nUpdate Email for all students");
			
			session.createQuery("update Student set email='foo@gmail.com'")
				.executeUpdate();
			
			//commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done");
			
		}
		finally {
			factory.close();
		}

	}

}
