package fr.cedricalibert.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			
			
			//start a transaction
			session.beginTransaction();
			
			//query students
			List<Student> students = session.createQuery("from Student").getResultList();
			
			//List students
			displayStudents(students);
			
			//query students: lastName="Doe"
			students = session.createQuery("from Student s where s.lastName = 'Doe'").getResultList();
			
			System.out.println("\nStudents with last name DOE");
			displayStudents(students);
			
			//query students: lastName="Doe" OR firstname = "Cédric"
			students = session.createQuery("from Student s where s.lastName = 'Doe'"
					+ "OR s.firstName = 'Paul'").getResultList();
			
			System.out.println("\nStudents with last name DOE OR firstname = Cédric");
			displayStudents(students);
			
			//query students: email LIKE "@test.fr"
			students = session.createQuery("from Student s where s.email LIKE '%@test.fr'").getResultList();
			
			System.out.println("\nStudents: email LIKE @test.fr");
			displayStudents(students);
			
			//commit transaction
			session.getTransaction().commit();
		
			
			System.out.println("Done");
			
		}
		finally {
			factory.close();
		}

	}

	private static void displayStudents(List<Student> students) {
		for (Student student : students) {
			System.out.println(student);
		}
	}

}
