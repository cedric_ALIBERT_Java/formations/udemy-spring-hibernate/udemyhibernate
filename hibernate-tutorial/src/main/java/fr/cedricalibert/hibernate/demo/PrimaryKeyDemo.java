package fr.cedricalibert.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Student;

public class PrimaryKeyDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			//use the session object to save Java object
			
			//create Three student object
			System.out.println("Create new student object");
			Student student1 = new Student("Cédric", "ALIBERT", "test1@test.fr");
			Student student2 = new Student("John", "Doe", "test2@test.fr");
			Student student3 = new Student("Paul", "Dupond", "test3@test.fr");
			
			//start a transaction
			session.beginTransaction();
			
			//save the students object
			System.out.println("Save the students");
			session.save(student1);
			session.save(student2);
			session.save(student3);
			
			//commit transaction
			session.getTransaction().commit();
			System.out.println("Done");
			
		}
		finally {
			factory.close();
		}

	}

}
